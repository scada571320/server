package api

import (
	"SCADA/internal/scada/models"
	"SCADA/internal/scada/storage"
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"log/slog"
	"net/http"
)

func CreateServerView(s *storage.Storage, l *slog.Logger) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var server models.Server
		err := json.NewDecoder(r.Body).Decode(&server)
		if err != nil {
			http.Error(w, "Bad request", http.StatusBadRequest)
			return
		}

		status, err := models.Create(&server, s, l)
		if err != nil {
			switch status {
			case 500:
				http.Error(w, "Internal server error", http.StatusInternalServerError)
			}
		}

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		err = json.NewEncoder(w).Encode(server)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	}
}

func GetServerListView(s *storage.Storage, l *slog.Logger) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		servers, status, err := models.GetServerList(s, l)
		if err != nil {
			switch status {
			case 500:
				http.Error(w, "Internal server error", http.StatusInternalServerError)
				return
			}
		}

		w.Header().Add("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(servers)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	}

}

func GetServerView(s *storage.Storage, l *slog.Logger) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "id")
		var server models.Server

		status, err := models.Get(&server, s, l, id)
		if err != nil {
			switch status {
			case 404:
				http.Error(w, "Server does not exist", http.StatusNotFound)
				return
			case 500:
				http.Error(w, "Internal server error", http.StatusInternalServerError)
				return
			}
		}

		w.Header().Add("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(server)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	}
}

func UpdateServerView(s *storage.Storage, l *slog.Logger) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var server models.Server
		err := json.NewDecoder(r.Body).Decode(&server)
		if err != nil {
			http.Error(w, "Bad request", http.StatusBadRequest)
			return
		}

		id := chi.URLParam(r, "id")
		status, err := models.Update(&server, s, l, id)
		if err != nil {
			switch status {
			case 404:
				http.Error(w, "Server does not exist", http.StatusNotFound)
				return
			case 500:
				http.Error(w, "Internal server error", http.StatusInternalServerError)
				return
			}
		}

		w.Header().Add("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(server)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	}
}

func DeleteServerView(s *storage.Storage, l *slog.Logger) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "id")

		status, err := models.DeleteServer(s, l, id)
		if err != nil {
			switch status {
			case 500:
				http.Error(w, "Internal server error", http.StatusInternalServerError)
				return
			}
		}

		w.WriteHeader(http.StatusNoContent)
	}
}
