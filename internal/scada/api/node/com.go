package node

import (
	"SCADA/internal/scada/models"
	"SCADA/internal/scada/models/node"
	"SCADA/internal/scada/storage"
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"github.com/go-playground/validator/v10"
	"log/slog"
	"net/http"
	"strconv"
)

func CreateCOMView(s *storage.Storage, l *slog.Logger) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		serverId := chi.URLParam(r, "server_id")
		comNode := node.New()
		validate := validator.New(validator.WithRequiredStructEnabled())

		err := json.NewDecoder(r.Body).Decode(comNode)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		comNode.ServerId, err = strconv.Atoi(serverId)

		if err != nil {
			http.Error(w, "Server does not exist", http.StatusNotFound)
			return
		}

		err = validate.Struct(comNode)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		status, err := models.Create(comNode, s, l)
		if err != nil {
			switch status {
			case 400:
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			case 500:
				http.Error(w, "Internal server error", http.StatusInternalServerError)
				return
			}
		}

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		err = json.NewEncoder(w).Encode(comNode)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	}
}

func GetListCOMView(s *storage.Storage, l *slog.Logger) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		serverId := chi.URLParam(r, "server_id")
		comNodes, status, err := node.GetListCOM(s, l, serverId)

		if err != nil {
			switch status {
			case 500:
				http.Error(w, "Internal server error", http.StatusInternalServerError)
				return
			}
		}

		w.Header().Add("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(comNodes)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	}
}

func GetDetailedCOMView(s *storage.Storage, l *slog.Logger) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "id")
		var comNode node.COMType
		status, err := models.Get(&comNode, s, l, id)
		if err != nil {
			switch status {
			case 404:
				http.Error(w, "COM node does not exist", http.StatusNotFound)
				return
			case 500:
				http.Error(w, "Internal server error", http.StatusInternalServerError)
				return
			}
		}

		w.Header().Add("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(comNode)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	}
}

func UpdateCOMView(s *storage.Storage, l *slog.Logger) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var comNode node.COMType
		status, err := models.Get(&comNode, s, l, chi.URLParam(r, "id"))
		if err != nil {
			switch status {
			case 404:
				http.Error(w, "COM node does not exist", http.StatusNotFound)
				return
			case 500:
				http.Error(w, "Internal server error", http.StatusInternalServerError)
				return
			}
		}

		var updateNode node.COMType
		err = json.NewDecoder(r.Body).Decode(&updateNode)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		status, err = comNode.Update(s, l, updateNode)
		if err != nil {
			switch status {
			case 404:
				http.Error(w, "COM node does not exist", http.StatusNotFound)
				return
			case 500:
				http.Error(w, "Internal server error", http.StatusInternalServerError)
				return
			}
		}

		w.Header().Add("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(comNode)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	}
}

func DeleteCOMView(s *storage.Storage, l *slog.Logger) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var comNode node.COMType
		status, err := models.Get(&comNode, s, l, chi.URLParam(r, "id"))
		if err != nil {
			switch status {
			case 404:
				http.Error(w, "COM node does not exist", http.StatusNotFound)
			case 500:
				http.Error(w, "Internal server error", http.StatusInternalServerError)
			}
		}

		status, err = node.DeleteCOM(s, l, chi.URLParam(r, "id"))
		if err != nil {
			switch status {
			case 500:
				http.Error(w, "Internal server error", http.StatusInternalServerError)
			}
		}

		w.WriteHeader(http.StatusNoContent)
	}
}
