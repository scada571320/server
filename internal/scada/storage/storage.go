package storage

import (
	"SCADA/configs"
	"SCADA/internal/scada/storage/postgres"
	"SCADA/internal/scada/storage/sqlite"
	"database/sql"
)

const (
	Postgres = "postgres"
	Sqlite   = "sqlite"
)

type Storage struct {
	SQLDb *sql.DB
}

func New(cfg *configs.Config) *Storage {
	storage := &Storage{}
	switch cfg.Database {
	case Sqlite:
		storage.SQLDb = sqlite.New(cfg)
	case Postgres:
		storage.SQLDb = postgres.New(cfg)
	}

	CreateServerTable(storage)
	CreateComType(storage)
	return storage
}

func CreateServerTable(db *Storage) {
	stmt, err := db.SQLDb.Prepare(`
CREATE TABLE IF NOT EXISTS servers (
		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		comment TEXT
	);
`)
	if err != nil {
		panic(err)
	}

	_, err = stmt.Exec()
	if err != nil {
		panic(err)
	}
}

func CreateComType(db *Storage) {
	stmt, err := db.SQLDb.Prepare(`
CREATE TABLE IF NOT EXISTS com_nodes (
    		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    		server_id INTEGER NOT NULL,
    		port INTEGER NOT NULL,
    		baud_rate INTEGER NOT NULL,
    		byte_size INTEGER NOT NULL,
    		stop_bits INTEGER NOT NULL,
    		parity TEXT NOT NULL,
    		comment TEXT DEFAULT "",
    		is_used BOOLEAN DEFAULT FALSE,
    		FOREIGN KEY (server_id) REFERENCES servers(id)
	);
`)
	if err != nil {
		panic(err)
	}

	_, err = stmt.Exec()
	if err != nil {
		panic(err)
	}
}
