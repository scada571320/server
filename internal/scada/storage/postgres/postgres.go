package postgres

import (
	"SCADA/configs"
	"database/sql"
	_ "github.com/lib/pq"
)

type DB struct {
	Db *sql.DB
}

func New(cfg *configs.Config) *sql.DB {
	db, err := sql.Open("postgres", cfg.Postgres.ConnectionString())
	if err != nil {
		panic(err)
	}

	return db
}
