package node

import (
	"encoding/json"
	"errors"
)

type BaudRate int

const (
	BaudRate9600   BaudRate = 9600
	BaudRate19200  BaudRate = 19200
	BaudRate38400  BaudRate = 38400
	BaudRate57600  BaudRate = 57600
	BaudRate115200 BaudRate = 115200
)

var valueToBaudRate = map[int]BaudRate{
	9600:   BaudRate9600,
	19200:  BaudRate19200,
	38400:  BaudRate38400,
	57600:  BaudRate57600,
	115200: BaudRate115200,
}

func (b *BaudRate) UnmarshalJSON(data []byte) error {
	var baudRateValue int
	if err := json.Unmarshal(data, &baudRateValue); err != nil {
		return err
	}

	baudRate, isValid := valueToBaudRate[baudRateValue]
	if !isValid {
		return errors.New("invalid baud rate")
	}

	*b = baudRate
	return nil
}

type ByteSize int

const (
	DataBits7 ByteSize = 7
	DataBits8 ByteSize = 8
)

var valueToDataBits = map[int]ByteSize{
	7: DataBits7,
	8: DataBits8,
}

func (d *ByteSize) UnmarshalJSON(data []byte) error {
	var dataBitsValue int
	if err := json.Unmarshal(data, &dataBitsValue); err != nil {
		return err
	}

	dataBits, isValid := valueToDataBits[dataBitsValue]
	if !isValid {
		return errors.New("invalid data bits")
	}

	*d = dataBits
	return nil
}

type StopBits int

const (
	StopBits0 StopBits = 0
	StopBits1 StopBits = 1
	StopBits2 StopBits = 2
)

var valueToStopBits = map[int]StopBits{
	0: StopBits0,
	1: StopBits1,
	2: StopBits2,
}

func (s *StopBits) UnmarshalJSON(data []byte) error {
	var stopBitsValue int
	if err := json.Unmarshal(data, &stopBitsValue); err != nil {
		return err
	}

	stopBits, isValid := valueToStopBits[stopBitsValue]
	if !isValid {
		return errors.New("invalid stop bits")
	}

	*s = stopBits
	return nil
}

type Parity string

const (
	ParityNone Parity = "none"
	ParityOdd  Parity = "odd"
	ParityEven Parity = "even"
)

var valueToParity = map[string]Parity{
	"none": ParityNone,
	"odd":  ParityOdd,
	"even": ParityEven,
}

func (p *Parity) UnmarshalJSON(data []byte) error {
	var parityValue string
	if err := json.Unmarshal(data, &parityValue); err != nil {
		return err
	}

	parity, isValid := valueToParity[parityValue]
	if !isValid {
		return errors.New("invalid parity")
	}

	*p = parity
	return nil
}
