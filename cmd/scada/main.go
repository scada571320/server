package main

import (
	"SCADA/configs"
	"SCADA/internal/scada/router"
	"SCADA/internal/scada/storage"
	"log/slog"
	"net/http"
	"os"
)

const (
	envDev  = "dev"
	envProd = "prod"
)

func main() {
	config := configs.NewConfig()
	logger := setupLogger(config.Env)

	logger.Info("Starting application...")
	logger.Debug("Config loaded: ", config)

	s := storage.New(config)

	srv := &http.Server{
		Addr:    ":8080",
		Handler: router.GetRouter(s, logger),
	}

	if err := srv.ListenAndServe(); err != nil {
		logger.Error("Server error: ", err)
	}
}

func setupLogger(env string) *slog.Logger {
	var logger *slog.Logger

	switch env {
	case envDev:
		logger = slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))
	case envProd:
		logger = slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}))
	}

	return logger
}
