package models

import (
	"SCADA/internal/scada/models/node"
	"SCADA/internal/scada/storage"
	"database/sql"
	"errors"
	"log/slog"
)

type Server struct {
	ID       int            `json:"id"`
	Comment  string         `json:"comment"`
	COMNodes []node.COMType `json:"com_nodes"`
	TCPNodes []node.TCPType `json:"tcp_nodes"`
}

func (s *Server) Create(st *storage.Storage, l *slog.Logger) (int, error) {
	op := "models.Server.Create"
	query := `INSERT INTO servers (comment) VALUES ($1) RETURNING id;`

	err := st.SQLDb.QueryRow(query, s.Comment).Scan(&s.ID)

	if err != nil {
		l.Error("error scanning row", "op", op, "err", err)
		return 500, err
	}

	return 201, nil
}

func (s *Server) Get(st *storage.Storage, l *slog.Logger, id string) (int, error) {
	op := "models.Server.Get"
	query := `SELECT s.id, s.comment, cn.id, cn.port, cn.baud_rate, cn.byte_size, cn.stop_bits, cn.parity FROM servers s LEFT JOIN com_nodes cn ON s.id = cn.server_id where s.id = $1;`
	rows, err := st.SQLDb.Query(query, id)

	if err != nil {
		l.Error("error querying rows", "op", op, "err", err)
		return 500, err
	}

	if !rows.Next() {
		return 404, errors.New("server not found")
	}

	for {
		var comNodeID, port, baudRate, byteSize, stopBits sql.NullInt64
		var parity sql.NullString
		err := rows.Scan(&s.ID, &s.Comment, &comNodeID, &port, &baudRate, &byteSize, &stopBits, &parity)

		if err != nil {
			l.Error("error scanning row", "op", op, "err", err)
			return 500, err
		}

		if comNodeID.Valid {
			comType := node.COMType{
				ID:       int(comNodeID.Int64),
				Port:     int(port.Int64),
				BaudRate: node.BaudRate(baudRate.Int64),
				ByteSize: node.ByteSize(byteSize.Int64),
				StopBits: node.StopBits(stopBits.Int64),
				Parity:   node.Parity(parity.String),
			}

			s.COMNodes = append(s.COMNodes, comType)
		} else {
			s.COMNodes = []node.COMType{{}}
		}

		if !rows.Next() {
			break
		}
	}

	return 200, nil
}

func (s *Server) Update(st *storage.Storage, l *slog.Logger, id string) (int, error) {
	op := "models.Server.Update"
	query := `UPDATE servers SET comment = $1 WHERE id = $2 RETURNING id;`
	err := st.SQLDb.QueryRow(query, s.Comment, id).Scan(&s.ID)

	if err != nil {
		l.Error("error executing statement", "op", op, "err", err)
		if errors.Is(err, sql.ErrNoRows) {
			return 404, err
		}
		return 500, err
	}

	return 200, nil
}

func DeleteServer(st *storage.Storage, l *slog.Logger, id string) (int, error) {
	op := "models.Server.Delete"
	query := `DELETE FROM servers WHERE id = $1;`
	stmt, err := st.SQLDb.Prepare(query)

	if err != nil {
		l.Error("error preparing statement", "op", op, "err", err)
		return 500, err
	}

	_, err = stmt.Exec(id)
	if err != nil {
		l.Error("error executing statement", "op", op, "err", err)
		return 500, err
	}

	return 204, nil
}

func GetServerList(st *storage.Storage, l *slog.Logger) ([]Server, int, error) {
	op := "models.Server.GetServerList"

	query := `SELECT s.id, s.comment, cn.id, cn.port, cn.baud_rate, cn.byte_size, cn.stop_bits, cn.parity FROM servers s LEFT JOIN com_nodes cn ON s.id = cn.server_id;`
	rows, err := st.SQLDb.Query(query)
	var servers []Server
	serversMap := make(map[int]Server)

	if err != nil {
		l.Error("error querying rows", "op", op, "err", err)
		return nil, 500, err
	}

	for rows.Next() {
		var serverID int
		var comment string
		var comNodeID, port, baudRate, byteSize, stopBits sql.NullInt64
		var parity sql.NullString
		err := rows.Scan(&serverID, &comment, &comNodeID, &port, &baudRate, &byteSize, &stopBits, &parity)
		if err != nil {
			l.Error("error scanning row", "op", op, "err", err)
			return nil, 500, err
		}

		if comNodeID.Valid {
			comType := node.COMType{
				ID:       int(comNodeID.Int64),
				Port:     int(port.Int64),
				BaudRate: node.BaudRate(baudRate.Int64),
				ByteSize: node.ByteSize(byteSize.Int64),
				StopBits: node.StopBits(stopBits.Int64),
				Parity:   node.Parity(parity.String),
			}

			if server, ok := serversMap[serverID]; ok {
				server.COMNodes = append(server.COMNodes, comType)
			} else {
				serversMap[serverID] = Server{
					ID:      serverID,
					Comment: comment,
					COMNodes: []node.COMType{
						comType,
					},
				}
			}
		} else {
			if _, ok := serversMap[serverID]; !ok {
				serversMap[serverID] = Server{
					ID:       serverID,
					Comment:  comment,
					COMNodes: []node.COMType{},
				}
			}

		}
	}

	for _, server := range serversMap {
		servers = append(servers, server)
	}

	return servers, 200, nil
}
