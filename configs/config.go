package configs

import (
	"gopkg.in/yaml.v3"
	"os"
)

type Postgres struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Database string `yaml:"database"`
}

type Sqlite struct {
	Path string `yaml:"path"`
}

type Config struct {
	Env      string `yaml:"env"`
	Database string `yaml:"database"`
	Postgres `yaml:"postgres"`
	Sqlite   `yaml:"sqlite"`
}

func NewConfig() *Config {
	cfg := &Config{}
	cfg.readConfig()
	return cfg
}

func (c *Config) readConfig() {
	filePath := "./configs/config.yml"
	file, err := os.ReadFile(filePath)
	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal(file, c)
	if err != nil {
		panic(err)
	}
}

func (d *Postgres) ConnectionString() string {
	return "host=" + d.Host +
		" port=" + d.Port +
		" user=" + d.User +
		" password=" + d.Password +
		" dbname=" + d.Database +
		" sslmode=disable"
}

func (d *Sqlite) ConnectionString() string {
	return d.Path
}
