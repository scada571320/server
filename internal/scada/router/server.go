package router

import (
	"SCADA/internal/scada/api"
	"SCADA/internal/scada/storage"
	"github.com/go-chi/chi/v5"
	"log/slog"
)

func ServerRoutes(s *storage.Storage, l *slog.Logger) func(r chi.Router) {
	return func(r chi.Router) {
		r.Post("/", api.CreateServerView(s, l))
		r.Get("/", api.GetServerListView(s, l))
		r.Get("/{id}", api.GetServerView(s, l))
		r.Patch("/{id}", api.UpdateServerView(s, l))
		r.Delete("/{id}", api.DeleteServerView(s, l))
	}
}
