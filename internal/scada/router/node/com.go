package node

import (
	"SCADA/internal/scada/api/node"
	"SCADA/internal/scada/storage"
	"github.com/go-chi/chi/v5"
	"log/slog"
)

func COMRoutes(s *storage.Storage, l *slog.Logger) func(r chi.Router) {
	return func(r chi.Router) {
		r.Post("/", node.CreateCOMView(s, l))
		r.Get("/", node.GetListCOMView(s, l))
		r.Get("/{id}", node.GetDetailedCOMView(s, l))
		r.Patch("/{id}", node.UpdateCOMView(s, l))
		r.Delete("/{id}", node.DeleteCOMView(s, l))
	}
}
