package sqlite

import (
	"SCADA/configs"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"os"
)

func New(cfg *configs.Config) *sql.DB {
	fileName := cfg.Sqlite.ConnectionString()
	f, err := os.OpenFile(fileName, os.O_CREATE, 0644)
	if err != nil {
		panic(err)
	}
	f.Close()
	db, err := sql.Open("sqlite3", fileName)
	if err != nil {
		panic(err)
	}
	return db
}
