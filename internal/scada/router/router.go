package router

import (
	"SCADA/internal/scada/router/node"
	"SCADA/internal/scada/storage"
	"github.com/go-chi/chi/v5"
	"log/slog"
)

func GetRouter(s *storage.Storage, l *slog.Logger) *chi.Mux {
	router := chi.NewRouter()
	router.Route("/servers", ServerRoutes(s, l))
	router.Route("/servers/{server_id}/nodes/com", node.COMRoutes(s, l))
	return router
}
