package node

import (
	"SCADA/internal/scada/storage"
	"database/sql"
	"errors"
	"fmt"
	"log/slog"
)

type COMType struct {
	ID       int      `json:"id"`
	Port     int      `json:"port,omitempty" validate:"required"`
	BaudRate BaudRate `json:"baud_rate,omitempty"`
	ByteSize ByteSize `json:"byte_size,omitempty"`
	StopBits StopBits `json:"stop_bits,omitempty"`
	Parity   Parity   `json:"parity,omitempty"`
	ServerId int      `json:"server_id,omitempty"`
	Node
}

func New() *COMType {
	return &COMType{
		BaudRate: BaudRate9600,
		ByteSize: DataBits8,
		StopBits: StopBits1,
		Parity:   ParityNone,
		Node:     Node{IsUsed: true},
	}
}

func (c *COMType) Create(s *storage.Storage, l *slog.Logger) (int, error) {
	op := "models.node.COMType.Create"

	query := `INSERT INTO com_nodes (server_id, port, baud_rate, byte_size, stop_bits, parity, is_used) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id;`

	err := s.SQLDb.QueryRow(query, c.ServerId, c.Port, c.BaudRate, c.ByteSize, c.StopBits, c.Parity, c.IsUsed).Scan(&c.ID)

	if err != nil {
		l.Error("error scanning row", "op", op, "err", err)
		return 500, err
	}

	return 201, nil
}

func (c *COMType) Get(s *storage.Storage, l *slog.Logger, id string) (int, error) {
	op := "models.node.COMType.Get"
	query := `SELECT id, server_id, port, baud_rate, byte_size, stop_bits, parity, comment, is_used FROM com_nodes WHERE id = $1;`

	err := s.SQLDb.QueryRow(query, id).Scan(&c.ID, &c.ServerId, &c.Port, &c.BaudRate, &c.ByteSize, &c.StopBits, &c.Parity, &c.Comment, &c.IsUsed)

	if err != nil {
		l.Error("error scanning row", "op", op, "err", err)
		if errors.Is(err, sql.ErrNoRows) {
			return 404, err
		}
		return 500, err
	}

	return 200, nil
}

func (c *COMType) Update(s *storage.Storage, l *slog.Logger, updateNode COMType) (int, error) {
	op := "models.node.COMType.Update"

	if updateNode.Port != 0 {
		c.Port = updateNode.Port
	}

	if updateNode.BaudRate != 0 {
		c.BaudRate = updateNode.BaudRate
	}

	if updateNode.ByteSize != 0 {
		c.ByteSize = updateNode.ByteSize
	}

	if updateNode.StopBits != 0 {
		c.StopBits = updateNode.StopBits
	}

	if updateNode.Parity != "" {
		c.Parity = updateNode.Parity
	}

	if updateNode.Comment != "" {
		c.Comment = updateNode.Comment
	}

	stmt, err := s.SQLDb.Prepare(`UPDATE com_nodes SET port = $1, baud_rate = $2, byte_size = $3, stop_bits = $4, parity = $5, comment = $6 WHERE id = $7;`)
	if err != nil {
		l.Error("error preparing statement", "op", op, "err", err)
		return 500, err
	}

	res, err := stmt.Exec(c.Port, c.BaudRate, c.ByteSize, c.StopBits, c.Parity, c.Comment, c.ID)

	if err != nil {
		l.Error("error executing query", "op", op, "err", err)
		return 500, err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		l.Error("error fetching rows affected", "op", op, "err", err)
		return 500, err
	}

	if rowsAffected == 0 {
		l.Warn("no rows updated", "op", op, "id", c.ID)
		return 404, fmt.Errorf("no rows updated")
	}

	return 200, nil
}

func GetListCOM(s *storage.Storage, l *slog.Logger, serverId string) ([]COMType, int, error) {
	op := "models.node.GetListCOM"
	query := `SELECT id, server_id, port, baud_rate, byte_size, stop_bits, parity, comment, is_used FROM com_nodes where server_id=$1;`
	rows, err := s.SQLDb.Query(query, serverId)

	if err != nil {
		l.Error("error querying rows", "op", op, "err", err)
		return nil, 500, err
	}

	var comNodes []COMType
	for rows.Next() {
		var comNode COMType
		err := rows.Scan(&comNode.ID, &comNode.ServerId, &comNode.Port, &comNode.BaudRate, &comNode.ByteSize, &comNode.StopBits, &comNode.Parity, &comNode.Comment, &comNode.IsUsed)

		if err != nil {
			l.Error("error scanning row", "op", op, "err", err)
			return nil, 500, err
		}

		comNodes = append(comNodes, comNode)
	}

	if comNodes == nil {
		comNodes = []COMType{}
	}

	return comNodes, 200, nil
}

func DeleteCOM(s *storage.Storage, l *slog.Logger, id string) (int, error) {
	op := "models.node.DeleteCOM"
	query := `DELETE FROM com_nodes WHERE id = $1;`
	stmt, err := s.SQLDb.Prepare(query)

	if err != nil {
		l.Error("error preparing statement", "op", op, "err", err)
		return 500, err
	}

	_, err = stmt.Exec(id)
	if err != nil {
		l.Error("error executing query", "op", op, "err", err)
		return 500, err
	}

	return 204, nil
}
