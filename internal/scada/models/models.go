package models

import (
	"SCADA/internal/scada/storage"
	"log/slog"
)

type Creator interface {
	Create(*storage.Storage, *slog.Logger) (int, error)
}

type Getter interface {
	Get(*storage.Storage, *slog.Logger, string) (int, error)
}

type Updater interface {
	Update(*storage.Storage, *slog.Logger, string) (int, error)
}

func Create(c Creator, s *storage.Storage, l *slog.Logger) (int, error) {
	return c.Create(s, l)
}

func Get(g Getter, s *storage.Storage, l *slog.Logger, id string) (int, error) {
	return g.Get(s, l, id)
}

func Update(u Updater, s *storage.Storage, l *slog.Logger, id string) (int, error) {
	return u.Update(s, l, id)
}
