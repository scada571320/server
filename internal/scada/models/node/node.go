package node

type Node struct {
	Comment string `json:"comment"`
	IsUsed  bool   `json:"is_used"`
}
